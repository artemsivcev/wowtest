package artemsivcev.wowtest;

import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.List;

/**
 * Created by artemsivcev on 07.10.17.
 */

public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {

    //start to load in background places info from google

    String googlePlacesData;
    String url;
    private MainActivity activityMain;

    public GetNearbyPlacesData(MainActivity activity) {
        this.activityMain = activity;
    }

    @Override
    protected String doInBackground(Object... params) {
        try {
            Log.d("GetNearbyPlacesData", "doInBackground entered");
            url = (String) params[0];
            DownloadUrl downloadUrl = new DownloadUrl();
            googlePlacesData = downloadUrl.readUrl(url);
            Log.d("GooglePlacesReadTask", "doInBackground Exit");
        } catch (Exception e) {
            Log.d("GooglePlacesReadTask", e.toString());
        }
        return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("GooglePlacesReadTask", "onPostExecute Entered");
        List<HashMap<String, String>> nearbyPlacesList = null;
        DataParser dataParser = new DataParser();
        nearbyPlacesList =  dataParser.parse(result);
        activityMain.setFoodList(nearbyPlacesList);
        Log.d("GooglePlacesReadTask", "onPostExecute Exit");
    }
}
