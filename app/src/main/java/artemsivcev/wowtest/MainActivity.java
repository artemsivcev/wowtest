package artemsivcev.wowtest;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    //Api controllers
    private GoogleApiClient mClient;
    GoogleMap mGMap;
    MapController mYaMapController;
    //default location is Sydney
    Double userLat = -33.852, userLon =  151.211;
    private int PROXIMITY_RADIUS = 10000;
    List<HashMap<String, String>> foodNearby = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Make 2 tabs to switch maps
        final TabHost tabHost = (TabHost) findViewById(R.id.thMaps);

        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("G_map_tag");
        tabSpec.setContent(R.id.tabGoogleMap);
        tabSpec.setIndicator(getString(R.string.google_map));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("Y_map_tag");
        tabSpec.setContent(R.id.tabYaMap);
        tabSpec.setIndicator(getString(R.string.ya_map));
        tabHost.addTab(tabSpec);

        //switch textColor when tabs changing
        TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
        tv.setTextColor(Color.parseColor("#dd443c"));

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {

                for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
                    TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
                    tv.setTextColor(Color.parseColor("#4a4a4a"));
                }

                TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
                tv.setTextColor(Color.parseColor("#dd443c"));

            }
        });
        //set default tab (google maps)
        tabHost.setCurrentTab(0);

        //want to permisson
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    42);
        }
        //configure maps
        configGMap();
        configYaMap();

        //check network state
        if(isNetworkAvailable(this)){
            buildGoogleApiClient();
        }else{
            Toast.makeText(MainActivity.this, "Network is unreachable. Check you connection and try again.", Toast.LENGTH_LONG).show();
        }

    }

    public void getRestaurantJson(Double Lat, Double Lon){
        String Restaurant = "restaurant";
        Log.d("onClick", "Button is Clicked");
        String url = getUrl(Lat, Lon, Restaurant);
        Object[] DataTransfer = new Object[1];
        DataTransfer[0] = url;
        Log.d("onClick", url);
        GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData(this);
        getNearbyPlacesData.execute(DataTransfer);
        Toast.makeText(MainActivity.this, "Nearby Restaurants", Toast.LENGTH_LONG).show();
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + nearbyPlace);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + "AIzaSyATuUiZUkEc_UgHuqsBJa1oqaODI-3mLs0");
        Log.d("getUrl", googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    private void ShowNearbyPlacesOnGMap(List<HashMap<String, String>> nearbyPlacesList) {
        for (int i = 0; i < nearbyPlacesList.size(); i++) {
            //add objects jn gmap
            Log.d("onPostExecute","Entered into showing locations");
            MarkerOptions markerOptions = new MarkerOptions();
            HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
            double lat = Double.parseDouble(googlePlace.get("lat"));
            double lng = Double.parseDouble(googlePlace.get("lng"));
            String placeName = googlePlace.get("place_name");
            String vicinity = googlePlace.get("vicinity");
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(placeName + " : " + vicinity);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_bar_black_18dp));
            mGMap.addMarker(markerOptions);
        }
        //move camera to you position
        LatLng youPosition = new LatLng(userLat, userLon);
        mGMap.addMarker(new MarkerOptions().position(youPosition)
                .title("You"));
        mGMap.animateCamera(CameraUpdateFactory.newLatLngZoom(youPosition, 10));

    }

    public void ShowNearbyPlacesOnYaMap(List<HashMap<String, String>> nearbyPlacesList) {
        if(mYaMapController != null) {
            for (int i = 0; i < nearbyPlacesList.size(); i++) {
                //ya map kit baloon add on layout and layout on map
                Log.d("onPostExecute","Entered into showing locations");
                HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
                double lat = Double.parseDouble(googlePlace.get("lat"));
                double lng = Double.parseDouble(googlePlace.get("lng"));
                String placeName = googlePlace.get("place_name");
                String vicinity = googlePlace.get("vicinity");

                BalloonItem balloonYandex = null;
                balloonYandex = new BalloonItem(this, new GeoPoint(lat, lng));
                balloonYandex.setText(placeName + " : " + vicinity);

                OverlayManager mOverlayManager = mYaMapController.getOverlayManager();
                Overlay overlay = new Overlay(mYaMapController);
                mOverlayManager.addOverlay(overlay);
                OverlayItem yandex = new OverlayItem(new GeoPoint(lat, lng), getResources().getDrawable(R.drawable.ic_local_bar_black_18dp));

                overlay.addOverlayItem(yandex);
                mOverlayManager.addOverlay(overlay);
                yandex.setBalloonItem(balloonYandex);
            }
        }
        //move camera to you position
        mYaMapController.setPositionAnimationTo(new GeoPoint(userLat, userLon));
        mYaMapController.setZoomCurrent(8);
    }

    public void setFoodList(List<HashMap<String, String>>  list) {
        this.foodNearby = list;
        if(foodNearby != null){

            ShowNearbyPlacesOnGMap(foodNearby);
            ShowNearbyPlacesOnYaMap(foodNearby);
        }
    }

    public void configGMap(){
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapG);
        mapFragment.getMapAsync(this);
    }

    public void configYaMap(){
        final MapView mMapView = (MapView) findViewById(R.id.mapYaKit);
        mYaMapController = mMapView.getMapController();
    }

    protected synchronized void buildGoogleApiClient() {
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(MainActivity.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mClient.connect();
    }

    @Override
    protected void onStop() {
        if(mClient != null){
            mClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGMap = googleMap;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        //on play services connected load places

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "Without location see Sydney!", Toast.LENGTH_SHORT).show();
        }

        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mClient);
        try{
            userLat = lastLocation.getLatitude();
            userLon = lastLocation.getLongitude();
        }catch (Exception e){
            Log.d("Exception", e.toString());
        }

        getRestaurantJson(userLat, userLon);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Need internet, or something went wrong, sorry!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 42:
                //check permissions
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.recreate();
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
